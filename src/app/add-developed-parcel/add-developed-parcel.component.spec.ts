import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDevelopedParcelComponent } from './add-developed-parcel.component';

describe('AddDevelopedParcelComponent', () => {
  let component: AddDevelopedParcelComponent;
  let fixture: ComponentFixture<AddDevelopedParcelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDevelopedParcelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDevelopedParcelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
