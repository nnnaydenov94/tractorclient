import { Component, OnInit } from '@angular/core';
import { HttpClientService, DevelopedParcel, DevelopedParcelInput } from '../service/http-client.service';


@Component({
  selector: 'app-add-developed-parcel',
  templateUrl: './add-developed-parcel.component.html',
  styleUrls: ['./add-developed-parcel.component.scss']
})
export class AddDevelopedParcelComponent implements OnInit {


  parcels: string[];
  tractors: string[];
  developedParcel: DevelopedParcel = new DevelopedParcel(null, "", "", null, "");
  developedParcelInput: DevelopedParcelInput = new DevelopedParcelInput(null, null, null, "");
  constructor(
    private httpClientService: HttpClientService
  ) { }

  ngOnInit() {
    this.httpClientService.getParcels().subscribe(
      parcelsResponse => this.handleSuccessfulParcelsResponse(parcelsResponse)
    );
    this.httpClientService.getTractors().subscribe(
      tractorsResponse => this.handleSuccessfulTractorsResponse(tractorsResponse)
    );
  }

  handleSuccessfulParcelsResponse(parcelsResponse){
    this.parcels = parcelsResponse;
  }
  handleSuccessfulTractorsResponse(tractorsResponse){
    this.tractors = tractorsResponse;
  }
  createDevelopedParcel(): void{
    this.httpClientService.createDevelopedParcel(this.developedParcelInput)
    .subscribe(data => {
      alert("Developed parcel created successfully.");
    }, error => alert(error.error) ) 
  }
}
