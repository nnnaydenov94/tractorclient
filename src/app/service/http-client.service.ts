import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export class Tractor {
  constructor(
    public id: number,
    public name: string
  ) { }
}

export class Parcel {
  constructor(
    public id: number,
    public name: string,
    public area: number
  ) { }
}

export class DevelopedParcel {
  constructor(
    public id: number,
    public tractorName: string,
    public parcelName: string,
    public developedArea: number,
    public date: string
  ) { }
}

export class DevelopedParcelInput {
  constructor(
    public parcelId: number,
    public tractorId: number,
    public developedArea: number,
    public date: string
  ) { }
}

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getTractors() {
    return this.httpClient.get<Tractor[]>('http://localhost:8085/tractors/get');
  }
  public createTractor(tractor) {
    return this.httpClient.post<Tractor>('http://localhost:8085/tractors/create', tractor);
  }

  getParcels() {
    return this.httpClient.get<Parcel[]>('http://localhost:8085/parcels/get');
  }

  public createParcel(parcel) {
    return this.httpClient.post<Parcel>('http://localhost:8085/parcels/create', parcel);
  }

  getDevelopedParcels() {
    return this.httpClient.get<DevelopedParcel[]>('http://localhost:8085/developedParcels/get');
  }

  public createDevelopedParcel(DevelopedParcelInput) {
    return this.httpClient.post<DevelopedParcel[]>('http://localhost:8085/developedParcels/create', DevelopedParcelInput);
  }
}
