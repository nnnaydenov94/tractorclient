import { Component, OnInit } from '@angular/core';
import { HttpClientService, DevelopedParcel } from '../service/http-client.service';
import { MatTableDataSource } from '@angular/material';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-developed-parcel',
  templateUrl: './developed-parcel.component.html',
  styleUrls: ['./developed-parcel.component.scss']
})
export class DevelopedParcelComponent implements OnInit {

  developedParcels: DevelopedParcel[];
  displayedColumns: string[] = ['tractorName', 'parcelName', 'developedArea', 'date'];
  total : number;
  dataSource: MatTableDataSource<DevelopedParcel>;
  constructor(
    private httpClientService:HttpClientService
  ) { }

  ngOnInit() {
    this.httpClientService.getDevelopedParcels().subscribe(
      response => this.handleSuccessfulResponse(response)
    );
  }
  handleSuccessfulResponse(response)
  {
    this.developedParcels=response;
    this.dataSource = new MatTableDataSource(this.developedParcels);
    this.total = this.developedParcels.map(d => d.developedArea).reduce((acc, value) => acc + value, 0);
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.total = this.dataSource.filteredData.map(d => d.developedArea).reduce((acc, value) => acc + value, 0);
  }
}
