import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TractorComponent} from './tractor/tractor.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { AuthGuardService } from './service/auth-guard.service';
import { AddTractorComponent } from './add-tractor/add-tractor.component';
import { AddParcelComponent } from './add-parcel/add-parcel.component';
import {ParcelComponent} from './parcel/parcel.component';
import { AddDevelopedParcelComponent } from './add-developed-parcel/add-developed-parcel.component';
import { DevelopedParcelComponent } from './developed-parcel/developed-parcel.component';


const routes: Routes = [
  {path:'tractors', component: TractorComponent, canActivate:[AuthGuardService] },
  {path:'login', component: LoginComponent},
  {path:'logout', component: LogoutComponent, canActivate:[AuthGuardService] },
  {path: 'add-tractor', component: AddTractorComponent, canActivate:[AuthGuardService]},
  {path: 'add-parcel', component: AddParcelComponent, canActivate:[AuthGuardService]},
  {path: 'parcels', component: ParcelComponent, canActivate:[AuthGuardService]},
  {path: 'add-developed-parcel', component: AddDevelopedParcelComponent, canActivate:[AuthGuardService]},
  {path: 'developed-parcels', component: DevelopedParcelComponent, canActivate:[AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
