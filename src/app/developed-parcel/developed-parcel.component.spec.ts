import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevelopedParcelComponent } from './developed-parcel.component';

describe('DevelopedParcelComponent', () => {
  let component: DevelopedParcelComponent;
  let fixture: ComponentFixture<DevelopedParcelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevelopedParcelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevelopedParcelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
