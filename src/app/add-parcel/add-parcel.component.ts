import { Component, OnInit } from '@angular/core';
import { HttpClientService, Parcel } from '../service/http-client.service';


@Component({
  selector: 'app-add-parcel',
  templateUrl: './add-parcel.component.html',
  styleUrls: ['./add-parcel.component.scss']
})
export class AddParcelComponent implements OnInit {

  parcel: Parcel = new Parcel( null, "", null);

  constructor(
    private httpClientService: HttpClientService
  ) { }

  ngOnInit() {
  }

  createParcel(): void{
    this.httpClientService.createParcel(this.parcel)
    .subscribe(data => {
      alert("Parcel created successfully.");
    },
      error => alert(error.error))
    }
  }

