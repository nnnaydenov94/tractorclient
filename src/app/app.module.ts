import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TractorComponent } from './tractor/tractor.component';
import { ParcelComponent } from './parcel/parcel.component';
import { DevelopedParcelComponent } from './developed-parcel/developed-parcel.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { HeaderComponent } from './header/header.component';
import { AddTractorComponent } from './add-tractor/add-tractor.component';
import { BasicAuthHttpInterceptorService } from './service/basic-auth-http-interceptor.service';
import { AddParcelComponent } from './add-parcel/add-parcel.component';
import { AddDevelopedParcelComponent } from './add-developed-parcel/add-developed-parcel.component';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import {MatOptionModule} from '@angular/material';
import {MatSelectModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import {MatButtonModule} from '@angular/material/button';



@NgModule({
  declarations: [
    AppComponent,
    TractorComponent,
    ParcelComponent,
    DevelopedParcelComponent,
    LoginComponent,
    LogoutComponent,
    HeaderComponent,
    AddTractorComponent,
    AddParcelComponent,
    AddDevelopedParcelComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule
    ],
  providers: [{
    provide: HTTP_INTERCEPTORS, useClass: BasicAuthHttpInterceptorService, multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
