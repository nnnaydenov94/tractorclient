import { Component, OnInit } from '@angular/core';
import { HttpClientService } from '../service/http-client.service';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-tractor',
  templateUrl: './tractor.component.html',
  styleUrls: ['./tractor.component.scss']
})
export class TractorComponent implements OnInit {

  tractors:string[];
  displayedColumns: string[] = ['name'];
  dataSource;

  constructor(
    private httpClientService:HttpClientService
  ) { }

  ngOnInit() {
    this.httpClientService.getTractors().subscribe(
      response => this.handleSuccessfulResponse(response)
    );
  }
  handleSuccessfulResponse(response)
{
    this.tractors=response;
    this.dataSource = new MatTableDataSource(this.tractors)
}
applyFilter(filterValue: string) {
  this.dataSource.filter = filterValue.trim().toLowerCase();
}
}
