import { Component, OnInit } from '@angular/core';
import { HttpClientService, Tractor } from '../service/http-client.service';

@Component({
  selector: 'app-add-tractor',
  templateUrl: './add-tractor.component.html',
  styleUrls: ['./add-tractor.component.scss']
})
export class AddTractorComponent implements OnInit {

  tractor: Tractor = new Tractor(null , "");

  constructor(private httpClientService: HttpClientService) { }

  ngOnInit() {
  }

  createTractor(): void{
    this.httpClientService.createTractor(this.tractor)
    .subscribe(data => {
      alert("Tractor created successfully.");
    }, error => alert(error.error))
  }
}
