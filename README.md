## Tractor frontend client

This is the frontend for the tractor application.
This can be run using ng serve.

Technological stack:
	1. Angular 7
	2. Material framework
	
Note:
User: user ; Password: password.
This is authenticated against the backend.
Before running the app you need to install angular material in the project.
This can be done using ' npm install --save @angular/material @angular/cdk @angular/animations '