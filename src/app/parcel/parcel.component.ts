import { Component, OnInit } from '@angular/core';
import { HttpClientService } from '../service/http-client.service';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-parcel',
  templateUrl: './parcel.component.html',
  styleUrls: ['./parcel.component.scss']
})
export class ParcelComponent implements OnInit {

  parcels: string[];
  displayedColumns: string[] = ['name', 'crop', 'area'];
  dataSource;

  constructor(
    private httpClientService: HttpClientService
  ) { }

  ngOnInit() {
    this.httpClientService.getParcels().subscribe(
      response => this.handleSuccessfulResponse(response)
    );
  }
  handleSuccessfulResponse(response) {
    this.parcels = response;
    this.dataSource = new MatTableDataSource(this.parcels);
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}
